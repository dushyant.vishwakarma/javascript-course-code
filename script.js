// console.log("Hello World!!!");

/*****************************
 *  Variables and data types
 *****************************/

// var firstName = "John";
// console.log(firstName);

// var lastName = "Doe";
// var age = 23;

// var isAdult = true;
// console.log(isAdult);

// var job;
// console.log(job);

// job = "Teacher";
// console.log(job);

/*****************************
 *  Variable mutation and type coercion
 *****************************/

//  var firstName = "John";
//  var age = 34;

// // Type Coercion
//  console.log(firstName + ' ' + age);

//  var job,isMarried;
//  job = "Teacher";
//  isMarried = false;

//  console.log(firstName + " is a " + age + " year old " + job + ". Is he married? " + isMarried);

//  // Variable Mutation
//  age = 24;                  // Re-declaring the age variable
//  job = "Web Developer";

//  alert(firstName + " is a " + age + " year old " + job + ". Is he married? " + isMarried);

//  var lastName = prompt("What is his last name ?");
//  alert("OK, So he is " + firstName + " " + lastName);

/*****************************
 *  Basic Operators
 *****************************/

//  var year = 2019;
//  var yearJohn = 23;
//  var yearMark = 28;

//  console.log(yearJohn);

//  console.log(year + 2);
//  console.log(year * 2);
//  console.log(year / 10);

//  // logical operators
//  var markOlder = yearJohn > yearMark;
//  console.log(markOlder);

//  var str = "Random String";
//  var x;

//  // typeof operator
//  console.log(typeof(markOlder));
//  console.log(typeof(yearJohn));
//  console.log(typeof(str));
//  console.log(typeof(x));

/*****************************
 *  Operator Precedence
 *****************************/

//  var year = 2019;
//  var yearMe = 1997;

//  // Multiple operators
//  var isAdult = year - yearMe >= 18;

//  console.log(isAdult);

//  // Grouping
//  var ageJohn = year - yearMe;
//  var ageMark = 35;

//  var avg = (ageJohn + ageMark) / 2;
//  console.log(avg);

//  // Multiple Assignments
//  var x,y;
//  x = y = (3+5) * 4 - 6;

//  console.log(x,y);

//  // More Operators
//  x *= 2;
//  console.log(x);

//  x += 10;
//  console.log(x);

//  x++;
//  console.log(x);

//  x--;
//  console.log(x);

/*****************************
 *  Coding Challenge 1 Solution
 *****************************/

// var massMark = 89;
// var massJohn = 78;

// var heightMark = 1.5;
// var heightJohn = 1.8;

// markBMI = massMark / (heightMark * heightMark);
// johnBMI = massJohn / (heightJohn * heightJohn);

// console.log(markBMI);
// console.log(johnBMI);

// var isHigherMark = markBMI > johnBMI;

// console.log("Is Mark's BMI higher than John's ? " + isHigherMark);

/*****************************
 *  IF ELSE Statements
 *****************************/

//  var firstName = "John";
//  var civilStatus = "Single";

//  if(civilStatus === "Married"){
//      console.log(firstName + " is married");
//  } else {
//      console.log(firstName + " will hopefully marry soon.");
//  }

//  var isMarried = true;

//  if(isMarried){
//     console.log(firstName + " is married");
// } else {
//     console.log(firstName + " will hopefully marry soon.");
// }

/*****************************
 *  Boolean Logic
 *****************************/

//  var firstName = "Mark";
//  var age = 16;

//  if(age < 13){
//      console.log(firstName + " is a boy.");
//  } else if(age >= 13 && age < 20){              // true
//     console.log(firstName + " is a teenager.");
//  } else {
//      console.log(firstName + " is a man.");
//  }

/*****************************
 *  Ternary Operator and Switch statement
 *****************************/

//  var firstName = "John";
//  var age = 15;

// // Ternary Operator
//  age >= 18 ? console.log(firstName + " drinks beer.") : console.log(firstName + " drinks juice.");

// // Switch statement

// var job = "Teacher";

// switch(job){
//     case 'Teacher':
//         console.log(firstName + " teaches students how to code.");
//         break;
//     case 'Driver':
//         console.log(firstName + " drives an uber in Bhopal.");
//         break;
//     case 'Designer':
//         console.log(firstName + " designs beatiful websites.");
//         break;
//     default:
//         console.log(firstName + " does something else.");

// }

/*****************************
 *  Truthy and Falsy values and equality operators
 *****************************/

// falsy values: undefined, null, 0 , '', NaN
// truthy values: NOT falsy values

//  var height = 23;

//  if(height || height === 0){
//      console.log(height + " is defined.");
//  } else {
//      console.log(height + " is undefined.");
//  }

//  // Equality operators
//  if(height == "23"){
//      console.log("The == operator does type coercion.");
//  }

/*****************************
 *  Coding challenge 2
 *****************************/

// var johnsTeamScore1 = 111;
// var johnsTeamScore2 = 111;
// var johnsTeamScore3 = 111;

// var avgJohnTeam = (johnsTeamScore1 + johnsTeamScore2 + johnsTeamScore3) / 3;

// var markTeamsScore1 = 111;
// var markTeamsScore2 = 111;
// var markTeamsScore3 = 111;

// var avgMarkTeam = (markTeamsScore1 + markTeamsScore2 + markTeamsScore3) / 3;

// var maryTeamScore1 = 111;
// var maryTeamScore2 = 111;
// var maryTeamScore3 = 111;

// var avgMaryTeam = (maryTeamScore1 + maryTeamScore2 + maryTeamScore3) / 3;

// // console.log(avgJohnTeam,avgMarkTeam);

// if(avgJohnTeam > avgMarkTeam && avgJohnTeam > avgMaryTeam){
//     console.log("John's team is the winner, with the average score of " + avgJohnTeam);
// } else if(avgMarkTeam > avgJohnTeam && avgMarkTeam > avgMaryTeam){
//     console.log("Mark's team is the winner, with the average score of " + avgMarkTeam);
// } else if(avgMaryTeam > avgMarkTeam && avgMaryTeam > avgJohnTeam){
//     console.log("Mary's team is the winner, with the average score of " + avgMaryTeam);
// } else {
//     console.log("There is a draw.");
// }

/*****************************
 *  Functions
 *****************************/

//  function calculateAge(birthYear){
//     return 2019 - birthYear;
//  }

//  var ageJohn = calculateAge(1990);
//  var ageMark = calculateAge(1997);
//  var ageJane = calculateAge(1968);

//  console.log(ageJohn, ageMark, ageJane);

//  // Function can call other functions

//  function yearsUntilRetirement(year,firstName){
//     var age = calculateAge(year);
//     var retirement = 65 - age;

//     if(retirement > 0){
//         console.log(firstName + " will retire in " + retirement + " years");
//     } else {
//         console.log(firstName + " is already retired.");
//     }
//  }

//  yearsUntilRetirement(1997,"Dushyant");
//  yearsUntilRetirement(1989,"Suhan");
//  yearsUntilRetirement(1953,"Sanya");

/*****************************
 *  Function statements and expressions
 *****************************/

// Function expression

//  var whatDoYouDo = function(job,firstName){
//     switch(job){
//         case 'Teacher':
//             return firstName + " teaches programming.";
//         case 'Driver':
//             return firstName + " drives an Uber in Mumbai.";
//         case 'Designer':
//             return firstName + " designs beautiful websites.";
//         default:
//             return firstName + " does something else.";
//     }
//  }

//  console.log(whatDoYouDo('Designer','Dushyant'));
//  console.log(whatDoYouDo('Teacher','Rajesh'));
//  console.log(whatDoYouDo('Nalle','Nishchay'));

/*****************************
 *  Arrays
 *****************************/

//  var names = ["John","Mark","Jane"];

//  console.log(names[0]);
//  console.log(names[1]);
//  console.log(names[2]);
//  console.log(names.length);

//  // Mutating array
//  names[1] = "Ben";
//  console.log(names);

//  // Adding an element to the array
//  names[names.length] = "Mary";
//  console.log(names);

//  // Different data types
//  var john = ["John","Smith",1990,"Teacher",false];

//  // Add an element to the array using the push method
//  john.push('blue');     // favorite color

//  // Add an element to the beginning of the array
//  john.unshift("Mr. ");
//  console.log(john);

//  // Remove an element from the end
//  john.pop();

//  // Remove the first element from the array
//  john.shift();

//  console.log(john);

//  // Return the position of an element in the array
//  console.log(john.indexOf(1990));

/*****************************
 *  Coding challenge 3
 *****************************/

//  function calculateTip(amount){
//     if(amount < 50){
//         return amount * 0.2;
//     } else if(amount >= 50 && amount <= 200){
//         return amount * 0.15;
//     } else {
//         return amount * 0.1;
//     }
//  }

//  var tips = [calculateTip(124),calculateTip(48),calculateTip(268)];
//  var paid_amounts = [124 + tips[0],48 + tips[1],268 + tips[2]];

//  console.log("Amount 1: " + paid_amounts[0] + " $");
//  console.log("Amount 2: " + paid_amounts[1] + " $");
//  console.log("Amount 3: " + paid_amounts[2] + " $");

/*****************************
 *  Objects and Properties
 *****************************/

// Example

//  var john = {
//      firstName : "John",
//      lastName : "Smith",
//      birthYear : 1997,
//      family : ["Jane","Mark","Bob","Emily"], // We can include arrays inside of objects
//      job : "Teacher",
//      isMarried : false
//  };

//  console.log(john.firstName);
//  console.log(john['lastName']);

// // Mutating an object's value

// john.job = "Designer";
// john['isMarried'] = true;
// console.log(john);

// var jane = new Object();
// jane.firstName = "Jane";
// jane.lastName = "Smith";
// jane.birthYear = 1996;
// jane.isMarried = false;
// jane.fullName = function(){
//     return this.firstName + " " + this.lastName;
// }

// console.log(jane.fullName());

/*****************************
 *  Objects and Methods
 *****************************/

// var jane = new Object();
// jane.firstName = "Jane";
// jane.lastName = "Smith";
// jane.birthYear = 1996;
// jane.isMarried = false;
// jane.fullName = function(){
//      return this.firstName + " " + this.lastName;
//     }

// var john = {
//     firstName : "John",
//     lastName: "Smith",
//     birthYear: 1990,
//     family: ["Jane","Mosh","Kane"],
//     job: "Teacher",
//     isMarried: false,
//     calculateAge: function(){
//         this.age = 2019 - this.birthYear;
//     }
// };

// // console.log(john.calculateAge());
// john.calculateAge();
// console.log(john);

/*****************************
 *  Coding Challenge 4
 *****************************/

//  var john = {
//     fullName : "John Smith",
//     mass: 87,
//     height: 2.0,
//     calculateBMI: function(){
//         this.BMI = this.mass / (this.height * this.height);
//         return this.BMI;
//     }
//  };

//  var mark = {
//     fullName : "Mark Geralt",
//     mass: 97,
//     height: 1.9,
//     calculateBMI: function(){
//         this.BMI = this.mass / (this.height * this.height);
//         return this.BMI;
//     }
//  };

//  if(john.calculateBMI() > mark.calculateBMI()){
//      console.log(john.fullName + " has higher BMI of " + john.BMI + " than " + mark.fullName);
//  } else if(mark.BMI > john.BMI){
//     console.log(mark.fullName + " has higher BMI of " + mark.BMI + " than " + john.fullName);
//  } else {
//      console.log("Both John's and Mark's BMI are same.");
//  }

/*****************************
 *  Loops & Iteration
 *****************************/
// for (var i = 0; i < 10; i++) {
//   console.log(i);
// }

var john = ["John", "Doe", 1996, "designer", false];

// for loop
// for (var i = 0; i < john.length; i++) {
//   console.log(john[i]);
// }

// while loop
// var i = 0;
// while (i < john.length) {
//   console.log(john[i]);
//   i++;
// }

// continue
// for (var i = 0; i < john.length; i++) {
//   if (typeof john[i] !== "string") continue; // !== is strict different operator ( read as different than)
//   console.log(john[i]);
// }

// // break
// for (var i = 0; i < john.length; i++) {
//   if (typeof john[i] !== "string") break; // break inside a loop exits or breaks out of the loop on a condition
//   console.log(john[i]);
// }

/*****************************
 *  CODING CHALLENGE 5
 *****************************/
// var john = {
//   fullName: "John Smith",
//   bills: [124, 48, 268, 180, 42],
//   tips: [],
//   finalAmount: [],
//   calucalteTip: function() {
//     for (var i = 0; i < this.bills.length; i++) {
//       // Determine the percentage based on tipping rules
//       var bill = this.bills[i];
//       var percentage;
//       if (bill < 50) {
//         percentage = 0.2;
//       } else if (bill >= 50 && bill <= 200) {
//         percentage = 0.15;
//       } else if (bill > 200) {
//         percentage = 0.1;
//       }

//       // Add results to the correspondin arrays
//       this.tips[i] = bill * percentage;
//       this.finalAmount[i] = bill + bill * percentage;
//     }
//   }
// };

// var mark = {
//   fullName: "Mark Miller",
//   bills: [77, 375, 110, 45],
//   tips: [],
//   finalAmount: [],
//   calucalteTip: function() {
//     for (var i = 0; i < this.bills.length; i++) {
//       // Determine the percentage based on tipping rules
//       var bill = this.bills[i];
//       var percentage;
//       if (bill < 50) {
//         percentage = 0.2;
//       } else if (bill >= 50 && bill <= 200) {
//         percentage = 0.15;
//       } else if (bill > 200) {
//         percentage = 0.1;
//       }

//       // Add results to the correspondin arrays
//       this.tips[i] = bill * percentage;
//       this.finalAmount[i] = bill + bill * percentage;
//     }
//   }
// };

// function calculateAverage(tips) {
//   var sum = 0;
//   for (var i = 0; i < tips.length; i++) {
//     sum = sum + tips[i];
//   }
//   return sum / tips.length;
// }

// john.calucalteTip();
// mark.calucalteTip();

// john.average = calculateAverage(john.tips);
// mark.average = calculateAverage(mark.tips);

// console.log(john, mark);

// if (john.average > mark.average) {
//   console.log(
//     john.fullName + " pays higher tips, with an average of $" + john.average
//   );
// } else if (mark.average > john.average) {
//   console.log(
//     mark.fullName + " pays higher tips, with an average of $" + mark.average
//   );
// }

/*****************************
 *  Hoisting in Practice
 *****************************/

// Functions Hoisting
// calculateAge(1997);

// function calculateAge(year) {
//   // The function declaration is stored in the Variable Object in the Creation Phase of Execution Context and even before the code is actually executed.
//   // So, when we enter the execution phase, the function is already available for us to use it. But this works only for function declarations.
//   console.log(2020 - year);
// }

// But this will not work for function expressions
// retirement(1965);

// var retirement = function(year) {
//   console.log(65 - (2020 - year));
// };

// Variables Hoisting

// console.log(age); // in the creation phase of variable object, the code is scanned for variable declaration and the variables are set to undefined.
// var age = 23; // this variable is stored in global execution context

// function sample() {
//   var age = 34; // This function gets it's own execution context
//   console.log(age);
// }

// sample();
// console.log(age);

/*****************************
 *  Scoping
 *****************************/

// var a = "Hello!";
// first();

// function first() {
//   var b = "Hi!";
//   second();

//   function second() {
//     var c = "Hey!";
//     console.log(a + b + c); // This works because of scoping chain
//   }
// }

/*****************************
 *  The 'this' keyword
 *****************************/

// console.log(this); // Here, this points to the window object, which is the default object

// calculateAge(1985);

// function calculateAge(year) {
//   console.log(2020 - year);
//   console.log(this); // Here, this also points to the window object, because it is a regular function call, not a method.
// }

// var john = {
//   name: "John",
//   yearOfBirth: 1990,
//   calculateAge: function() {
//     console.log(2020 - this.yearOfBirth);
//     console.log(this); // Here, 'this' points to the john object, because it is a method of john object.
//   }
// };

// john.calculateAge();

// var mike = {
//   name: "Mike",
//   yearOfBirth: 1985
// };

// // Method Borrowing
// mike.calculateAge = john.calculateAge; // Treating a function as a variable
// mike.calculateAge();
